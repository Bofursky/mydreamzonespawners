package net.myotherworld.Spawners.Data;

import org.bukkit.configuration.file.YamlConfiguration;

public class ConfigData 
{	
	public boolean GiveSpawner;
	public double PriceBat;
	public double PriceBlaze;
	public double PriceCaveSpider;
	public double PriceChicken;
	public double PriceCow;
	public double PriceCrepper;
	public double PriceEnderDragon;
	public double PriceEnderman;
	public double PriceGhast;
	public double PriceGiant;
	public double PriceGuardian;
	public double PriceIronGolem;
	public double PriceMagmaCube;
	public double PriceMushroomCow;
	public double PriceOcelot;
	public double PricePig;
	public double PricePigZombie;
	public double PriceRabbit;
	public double PriceSheep;
	public double PriceSilverFish;
	public double PriceSkeleton;
	public double PriceSlime;
	public double PriceSnowMan;
	public double PriceSpider;
	public double PriceSquid;
	public double PriceVillager;
	public double PriceWitch;
	public double PriceWither;
	public double PriceWolf;
	public double PriceZombie;
	
	public ConfigData(YamlConfiguration config) 
	{
		GiveSpawner = config.getBoolean("GiveSpawner", true);
		
		PriceBat = config.getLong("Price.Bat" , 0);
		PriceBlaze = config.getLong("Price.Blaze" , 0);
		PriceCaveSpider = config.getLong("Price.CaveSpider" , 0);
		PriceChicken = config.getLong("Price.Chicken" , 0);
		PriceCow = config.getLong("Price.Cow" , 0);
		PriceCrepper = config.getLong("Price.Crepper" , 0);
		PriceEnderDragon = config.getLong("Price.EnderDragon" , 0);
		PriceEnderman = config.getLong("Price.Enderman" , 0);
		PriceGhast = config.getLong("Price.Ghast" , 0);
		PriceGiant = config.getLong("Price.Giant" , 0);
		PriceGuardian = config.getLong("Price.Guardian" , 0);
		PriceIronGolem = config.getLong("Price.IronGolem" , 0);
		PriceMagmaCube = config.getLong("Price.MagmaCube" , 0);
		PriceMushroomCow = config.getLong("Price.MushroomCow" , 0);
		PriceOcelot = config.getLong("Price.Ocelot" , 0);
		PricePig = config.getLong("Price.Pig" , 0);
		PricePigZombie = config.getLong("Price.PigZombie" , 0);
		PriceRabbit = config.getLong("Price.Rabbit" , 0);
		PriceSheep = config.getLong("Price.Sheep" , 0);
		PriceSilverFish = config.getLong("Price.SilverFish" , 0);
		PriceSkeleton = config.getLong("Price.Skeleton" , 0);
		PriceSlime = config.getLong("Price.Slime" , 0);
		PriceSnowMan = config.getLong("Price.SnowMan" , 0);
		PriceSpider = config.getLong("Price.Spider" , 0);
		PriceSquid = config.getLong("Price.Squid" , 0);
		PriceVillager = config.getLong("Price.Villager" , 0);
		PriceWitch = config.getLong("Price.Witch" , 0);
		PriceWither = config.getLong("Price.Wither" , 0);
		PriceWolf = config.getLong("Price.Wolf" , 0);
		PriceZombie = config.getLong("Price.Zombie" , 0);
	}
}
