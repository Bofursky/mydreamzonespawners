package net.myotherworld.Spawners.Data.Listeners;

import java.util.HashSet;

import net.milkbowl.vault.economy.EconomyResponse;
import net.myotherworld.Spawners.Spawners;

import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.block.CreatureSpawner;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.ItemStack;

public class SpawnersSetListener implements Listener
{
	private Spawners plugin;
	
	public SpawnersSetListener(Spawners plugin)
	{
		this.plugin = plugin;
	}
	@EventHandler
	public void Move(InventoryClickEvent event)
	{
    	if (event.getInventory() == null)
    	{
    		return;		
    	}
    	if (event.getInventory().getName().equalsIgnoreCase(plugin.message.Menu))
    	{
    		event.setCancelled(true);
    	}
	}
	@EventHandler
	public void Spawner(BlockBreakEvent event)
	{
		Player p = event.getPlayer();
		if(plugin.config.GiveSpawner == true && p.hasPermission("MyOtherWorldSpawners.GiveSpawner"))
		{
			if(event.getBlock().getType().equals(Material.MOB_SPAWNER))
			{
				if(event.getPlayer().getItemInHand().getType() == Material.DIAMOND_PICKAXE)
				{
					if(event.getPlayer().getItemInHand().containsEnchantment(Enchantment.SILK_TOUCH))
					{					
						ItemStack IS = new ItemStack(Material.MOB_SPAWNER);
						p.getInventory().addItem(IS);
					}
				}
			}
		}
	}
	@EventHandler
	public void Click(InventoryClickEvent event)
	{
    	if (event.getInventory() == null)
    	{
    		return;		
    	}
    	if (event.getInventory().getName().equalsIgnoreCase(plugin.message.Menu))
    	{
    		try
    		{	
    			Player p = (Player) event.getWhoClicked();
    			@SuppressWarnings("deprecation")
				Block b = p.getTargetBlock((HashSet<Byte>) null, 10);	
    			if(b.getType().equals(Material.MOB_SPAWNER))
    			{
	    			if (event.getCurrentItem().getItemMeta().getDisplayName().equalsIgnoreCase(plugin.message.Bat))
	    	        {	
	    				if(!p.hasPermission("MyOtherWorldSpawners.free.bat")) 
	    				{
	    					EconomyResponse r = plugin.economy.withdrawPlayer(p, plugin.config.PriceBat);   			
		    				if (r.transactionSuccess())
		    				{
			    				CreatureSpawner s = (CreatureSpawner) b.getState();
			    				s.setSpawnedType(EntityType.BAT);
			    				s.update();
			    				p.closeInventory();
			    				p.sendMessage(plugin.message.Prefix + plugin.message.SpawnerSet + EntityType.BAT );    	
		    				}
		    				else 
		    				{
		    					p.closeInventory();
		    					p.sendMessage(plugin.message.Prefix + plugin.message.Money);
		    				}
	    				}
	    				else
	    				{
		    				CreatureSpawner s = (CreatureSpawner) b.getState();
		    				s.setSpawnedType(EntityType.BAT);
		    				s.update();
		    				p.closeInventory();
		    				p.sendMessage(plugin.message.Prefix + plugin.message.SpawnerSet + EntityType.BAT );  			
	    				}
	    	        }    			
	    			if (event.getCurrentItem().getItemMeta().getDisplayName().equalsIgnoreCase(plugin.message.Blaze))
	    	        {	
	    				if(!p.hasPermission("MyOtherWorldSpawners.free.blaze")) 
	    				{
		    				EconomyResponse r = plugin.economy.withdrawPlayer(p, plugin.config.PriceBlaze);
		    				if (r.transactionSuccess())
		    				{
			    				CreatureSpawner s = (CreatureSpawner) b.getState();
			    				s.setSpawnedType(EntityType.BLAZE);
			    				s.update();
			    				p.closeInventory();
			    				p.sendMessage(plugin.message.Prefix + plugin.message.SpawnerSet + EntityType.BLAZE );    	
		    				}
		    				else 
		    				{
		    					p.closeInventory();
		    					p.sendMessage(plugin.message.Prefix + plugin.message.Money);
		    				}
	    				}
	    				else
	    				{
		    				CreatureSpawner s = (CreatureSpawner) b.getState();
		    				s.setSpawnedType(EntityType.BLAZE);
		    				s.update();
		    				p.closeInventory();
		    				p.sendMessage(plugin.message.Prefix + plugin.message.SpawnerSet + EntityType.BLAZE );  			
	    				}
	    	        }
	    			if (event.getCurrentItem().getItemMeta().getDisplayName().equalsIgnoreCase(plugin.message.CaveSpider))
	    	        {	    				
	    				if(!p.hasPermission("MyOtherWorldSpawners.free.cavespider")) 
	    				{
		    				EconomyResponse r = plugin.economy.withdrawPlayer(p, plugin.config.PriceCaveSpider);
		    				if (r.transactionSuccess())
		    				{
			    				CreatureSpawner s = (CreatureSpawner) b.getState();
			    				s.setSpawnedType(EntityType.CAVE_SPIDER);
			    				s.update();
			    				p.closeInventory();
			    				p.sendMessage(plugin.message.Prefix + plugin.message.SpawnerSet + EntityType.CAVE_SPIDER );  
		    				}
		    				else 
		    				{
		    					p.closeInventory();
		    					p.sendMessage(plugin.message.Prefix + plugin.message.Money);
		    				}
	    				}
	    				else
	    				{
		    				CreatureSpawner s = (CreatureSpawner) b.getState();
		    				s.setSpawnedType(EntityType.CAVE_SPIDER);
		    				s.update();
		    				p.closeInventory();
		    				p.sendMessage(plugin.message.Prefix + plugin.message.SpawnerSet + EntityType.CAVE_SPIDER );  			
	    				}
	    	        }
	    			if (event.getCurrentItem().getItemMeta().getDisplayName().equalsIgnoreCase(plugin.message.Chicken))
	    	        {	
	    				if(!p.hasPermission("MyOtherWorldSpawners.free.chicken")) 
	    				{
		    				EconomyResponse r = plugin.economy.withdrawPlayer(p, plugin.config.PriceChicken);
		    				if (r.transactionSuccess())
		    				{
			    				CreatureSpawner s = (CreatureSpawner) b.getState();
			    				s.setSpawnedType(EntityType.CHICKEN);
			    				s.update();
			    				p.closeInventory();
			    				p.sendMessage(plugin.message.Prefix + plugin.message.SpawnerSet + EntityType.CHICKEN );
		    				}
		    				else 
		    				{
		    					p.closeInventory();
		    					p.sendMessage(plugin.message.Prefix + plugin.message.Money);
		    				}
	    				}
	    				else
	    				{
		    				CreatureSpawner s = (CreatureSpawner) b.getState();
		    				s.setSpawnedType(EntityType.CHICKEN);
		    				s.update();
		    				p.closeInventory();
		    				p.sendMessage(plugin.message.Prefix + plugin.message.SpawnerSet + EntityType.CHICKEN );  			
	    				}		    				
	    	        }
	    			if (event.getCurrentItem().getItemMeta().getDisplayName().equalsIgnoreCase(plugin.message.Cow))
	    	        {	
	    				if(!p.hasPermission("MyOtherWorldSpawners.free.cow")) 
	    				{
		    				EconomyResponse r = plugin.economy.withdrawPlayer(p, plugin.config.PriceCow);
		    				if (r.transactionSuccess())
		    				{
			    				CreatureSpawner s = (CreatureSpawner) b.getState();
			    				s.setSpawnedType(EntityType.COW);
			    				s.update();
			    				p.closeInventory();
			    				p.sendMessage(plugin.message.Prefix + plugin.message.SpawnerSet + EntityType.COW );
		    				}
		    				else 
		    				{
		    					p.closeInventory();
		    					p.sendMessage(plugin.message.Prefix + plugin.message.Money);
		    				}
	    				}
	    				else
	    				{
		    				CreatureSpawner s = (CreatureSpawner) b.getState();
		    				s.setSpawnedType(EntityType.COW);
		    				s.update();
		    				p.closeInventory();
		    				p.sendMessage(plugin.message.Prefix + plugin.message.SpawnerSet + EntityType.COW );  			
	    				}
	    	        }
	    			if (event.getCurrentItem().getItemMeta().getDisplayName().equalsIgnoreCase(plugin.message.Crepper))
	    	        {	
	    				if(!p.hasPermission("MyOtherWorldSpawners.free.crepper")) 
	    				{
		    				EconomyResponse r = plugin.economy.withdrawPlayer(p, plugin.config.PriceCrepper);
		    				if (r.transactionSuccess())
		    				{
			    				CreatureSpawner s = (CreatureSpawner) b.getState();
			    				s.setSpawnedType(EntityType.CREEPER);
			    				s.update();
			    				p.closeInventory();
			    				p.sendMessage(plugin.message.Prefix + plugin.message.SpawnerSet + EntityType.CREEPER );
		    				}
		    				else 
		    				{
		    					p.closeInventory();
		    					p.sendMessage(plugin.message.Prefix + plugin.message.Money);
		    				}
	    				}
	    				else
	    				{
		    				CreatureSpawner s = (CreatureSpawner) b.getState();
		    				s.setSpawnedType(EntityType.CREEPER);
		    				s.update();
		    				p.closeInventory();
		    				p.sendMessage(plugin.message.Prefix + plugin.message.SpawnerSet + EntityType.CREEPER );  			
	    				}
	    	        }
	    			if (event.getCurrentItem().getItemMeta().getDisplayName().equalsIgnoreCase(plugin.message.EnderDragon))
	    	        {	
	    				if(!p.hasPermission("MyOtherWorldSpawners.free.enderdragon")) 
	    				{
		    				EconomyResponse r = plugin.economy.withdrawPlayer(p, plugin.config.PriceEnderDragon);
		    				if (r.transactionSuccess())
		    				{
			    				CreatureSpawner s = (CreatureSpawner) b.getState();
			    				s.setSpawnedType(EntityType.ENDER_DRAGON);
			    				s.update();
			    				p.closeInventory();
			    				p.sendMessage(plugin.message.Prefix + plugin.message.SpawnerSet + EntityType.ENDER_DRAGON );    
		    				}
			   				else 
			    			{
			    				p.closeInventory();
			    				p.sendMessage(plugin.message.Prefix + plugin.message.Money);
			    			}
	    				}
	    				else
	    				{
		    				CreatureSpawner s = (CreatureSpawner) b.getState();
		    				s.setSpawnedType(EntityType.ENDER_DRAGON);
		    				s.update();
		    				p.closeInventory();
		    				p.sendMessage(plugin.message.Prefix + plugin.message.SpawnerSet + EntityType.ENDER_DRAGON );  			
	    				}
	    	        }
	    			if (event.getCurrentItem().getItemMeta().getDisplayName().equalsIgnoreCase(plugin.message.Enderman))
	    	        {	
	    				if(!p.hasPermission("MyOtherWorldSpawners.free.enderman")) 
	    				{
		    				EconomyResponse r = plugin.economy.withdrawPlayer(p, plugin.config.PriceEnderman);
		    				if (r.transactionSuccess())
		    				{
			    				CreatureSpawner s = (CreatureSpawner) b.getState();
			    				s.setSpawnedType(EntityType.ENDERMAN);
			    				s.update();
			    				p.closeInventory();
			    				p.sendMessage(plugin.message.Prefix + plugin.message.SpawnerSet + EntityType.ENDERMAN );   
		    				}
			   				else 
			    			{
			    				p.closeInventory();
			    				p.sendMessage(plugin.message.Prefix + plugin.message.Money);
			    			}	
	    				}
	    				else
	    				{
		    				CreatureSpawner s = (CreatureSpawner) b.getState();
		    				s.setSpawnedType(EntityType.ENDERMAN);
		    				s.update();
		    				p.closeInventory();
		    				p.sendMessage(plugin.message.Prefix + plugin.message.SpawnerSet + EntityType.ENDERMAN );  			
	    				}
	    	        }    			
	    			if (event.getCurrentItem().getItemMeta().getDisplayName().equalsIgnoreCase(plugin.message.Ghast))
	    	        {
	    				if(!p.hasPermission("MyOtherWorldSpawners.free.ghast")) 
	    				{
		    				event.setCancelled(true);
		    				EconomyResponse r = plugin.economy.withdrawPlayer(p, plugin.config.PriceGhast);
		    				if (r.transactionSuccess())
		    				{
			    				CreatureSpawner s = (CreatureSpawner) b.getState();
			    				s.setSpawnedType(EntityType.GHAST);
			    				s.update();
			    				p.closeInventory();
			    				p.sendMessage(plugin.message.Prefix + plugin.message.SpawnerSet + EntityType.GHAST );   
		    				}
			   				else 
			    			{
			    				p.closeInventory();
			    				p.sendMessage(plugin.message.Prefix + plugin.message.Money);
			    			}
	    				}
	    				else
	    				{
		    				CreatureSpawner s = (CreatureSpawner) b.getState();
		    				s.setSpawnedType(EntityType.GHAST);
		    				s.update();
		    				p.closeInventory();
		    				p.sendMessage(plugin.message.Prefix + plugin.message.SpawnerSet + EntityType.GHAST );  			
	    				}	    					    				
	    	        }  
	    			if (event.getCurrentItem().getItemMeta().getDisplayName().equalsIgnoreCase(plugin.message.Giant))
	    	        {	
	    				if(!p.hasPermission("MyOtherWorldSpawners.free.giant")) 
	    				{
		    				EconomyResponse r = plugin.economy.withdrawPlayer(p, plugin.config.PriceGiant);
		    				if (r.transactionSuccess())
		    				{
			    				CreatureSpawner s = (CreatureSpawner) b.getState();
			    				s.setSpawnedType(EntityType.GIANT);
			    				s.update();
			    				p.closeInventory();
			    				p.sendMessage(plugin.message.Prefix + plugin.message.SpawnerSet + EntityType.GIANT );   
		    				}
			   				else 
			    			{
			    				p.closeInventory();
			    				p.sendMessage(plugin.message.Prefix + plugin.message.Money);
			    			}
	    				}
	    				else
	    				{
		    				CreatureSpawner s = (CreatureSpawner) b.getState();
		    				s.setSpawnedType(EntityType.GIANT);
		    				s.update();
		    				p.closeInventory();
		    				p.sendMessage(plugin.message.Prefix + plugin.message.SpawnerSet + EntityType.GIANT );  			
	    				}   				
	    	        }  
	    			if (event.getCurrentItem().getItemMeta().getDisplayName().equalsIgnoreCase(plugin.message.Guardian))
	    	        {
    			    	if(plugin.mode1_8)
    			    	{
		    				if(!p.hasPermission("MyOtherWorldSpawners.free.guardian")) 
		    				{
			    				EconomyResponse r = plugin.economy.withdrawPlayer(p, plugin.config.PriceGuardian);
			    				if (r.transactionSuccess())
			    				{
				    				CreatureSpawner s = (CreatureSpawner) b.getState();
				    				s.setSpawnedType(EntityType.GUARDIAN);
				    				s.update();
				    				p.closeInventory();
				    				p.sendMessage(plugin.message.Prefix + plugin.message.SpawnerSet + EntityType.GUARDIAN );   
			    				}
				   				else 
				    			{
				    				p.closeInventory();
				    				p.sendMessage(plugin.message.Prefix + plugin.message.Money);
				    			}
		    				}
		    				else
		    				{
			    				CreatureSpawner s = (CreatureSpawner) b.getState();
			    				s.setSpawnedType(EntityType.GUARDIAN);
			    				s.update();
			    				p.closeInventory();
			    				p.sendMessage(plugin.message.Prefix + plugin.message.SpawnerSet + EntityType.GUARDIAN );  			
		    				}
    			    	}
    			    	else
    			    	{
    			    		p.closeInventory();
    			    		p.sendMessage("Only Spigot 1.8");   			    		
    			    	}
	    	        } 
	    			if (event.getCurrentItem().getItemMeta().getDisplayName().equalsIgnoreCase(plugin.message.IronGolem))
	    	        {	
	    				if(!p.hasPermission("MyOtherWorldSpawners.free.irongolem")) 
	    				{
		    				EconomyResponse r = plugin.economy.withdrawPlayer(p, plugin.config.PriceIronGolem);
		    				if (r.transactionSuccess())
		    				{
			    				CreatureSpawner s = (CreatureSpawner) b.getState();
			    				s.setSpawnedType(EntityType.IRON_GOLEM);
			    				s.update();
			    				p.closeInventory();
			    				p.sendMessage(plugin.message.Prefix + plugin.message.SpawnerSet + EntityType.IRON_GOLEM );   
		    				}
			   				else 
			    			{
			    				p.closeInventory();
			    				p.sendMessage(plugin.message.Prefix + plugin.message.Money);
			    			}
	    				}
	    				else
	    				{
		    				CreatureSpawner s = (CreatureSpawner) b.getState();
		    				s.setSpawnedType(EntityType.IRON_GOLEM);
		    				s.update();
		    				p.closeInventory();
		    				p.sendMessage(plugin.message.Prefix + plugin.message.SpawnerSet + EntityType.IRON_GOLEM );  			
	    				}		    				
	    	        } 
	    			if (event.getCurrentItem().getItemMeta().getDisplayName().equalsIgnoreCase(plugin.message.MagmaCube))
	    	        {	
	    				if(!p.hasPermission("MyOtherWorldSpawners.free.magmacube")) 
	    				{
		    				EconomyResponse r = plugin.economy.withdrawPlayer(p, plugin.config.PriceMagmaCube);
		    				if (r.transactionSuccess())
		    				{
			    				CreatureSpawner s = (CreatureSpawner) b.getState();
			    				s.setSpawnedType(EntityType.MAGMA_CUBE);
			    				s.update();
			    				p.closeInventory();
			    				p.sendMessage(plugin.message.Prefix + plugin.message.SpawnerSet + EntityType.MAGMA_CUBE );   
		    				}
			   				else 
			    			{
			    				p.closeInventory();
			    				p.sendMessage(plugin.message.Prefix + plugin.message.Money);
			    			}	
	    				}
	    				else
	    				{
		    				CreatureSpawner s = (CreatureSpawner) b.getState();
		    				s.setSpawnedType(EntityType.MAGMA_CUBE);
		    				s.update();
		    				p.closeInventory();
		    				p.sendMessage(plugin.message.Prefix + plugin.message.SpawnerSet + EntityType.MAGMA_CUBE );  			
	    				}
	    	        } 
	    			if (event.getCurrentItem().getItemMeta().getDisplayName().equalsIgnoreCase(plugin.message.MushroomCow))
	    	        {	
	    				if(!p.hasPermission("MyOtherWorldSpawners.free.mushroomcow")) 
	    				{
		    				EconomyResponse r = plugin.economy.withdrawPlayer(p, plugin.config.PriceMushroomCow);
		    				if (r.transactionSuccess())
		    				{
			    				CreatureSpawner s = (CreatureSpawner) b.getState();
			    				s.setSpawnedType(EntityType.MUSHROOM_COW);
			    				s.update();
			    				p.closeInventory();
			    				p.sendMessage(plugin.message.Prefix + plugin.message.SpawnerSet + EntityType.MUSHROOM_COW );   
		    				}
			   				else 
			    			{
			    				p.closeInventory();
			    				p.sendMessage(plugin.message.Prefix + plugin.message.Money);
			    			} 
	    				}
	    				else
	    				{
		    				CreatureSpawner s = (CreatureSpawner) b.getState();
		    				s.setSpawnedType(EntityType.MUSHROOM_COW);
		    				s.update();
		    				p.closeInventory();
		    				p.sendMessage(plugin.message.Prefix + plugin.message.SpawnerSet + EntityType.MUSHROOM_COW );  			
	    				}
	    	        } 
	    			if (event.getCurrentItem().getItemMeta().getDisplayName().equalsIgnoreCase(plugin.message.Ocelot))
	    	        {	
	    				if(!p.hasPermission("MyOtherWorldSpawners.free.ocelot")) 
	    				{
		    				EconomyResponse r = plugin.economy.withdrawPlayer(p, plugin.config.PriceOcelot);
		    				if (r.transactionSuccess())
		    				{
			    				CreatureSpawner s = (CreatureSpawner) b.getState();
			    				s.setSpawnedType(EntityType.OCELOT);
			    				s.update();
			    				p.closeInventory();
			    				p.sendMessage(plugin.message.Prefix + plugin.message.SpawnerSet + EntityType.OCELOT );   
		    				}
			   				else 
			    			{
			    				p.closeInventory();
			    				p.sendMessage(plugin.message.Prefix + plugin.message.Money);
			    			}
	    				}
	    				else
	    				{
		    				CreatureSpawner s = (CreatureSpawner) b.getState();
		    				s.setSpawnedType(EntityType.OCELOT);
		    				s.update();
		    				p.closeInventory();
		    				p.sendMessage(plugin.message.Prefix + plugin.message.SpawnerSet + EntityType.OCELOT );  			
	    				}
	    	        } 
	    			if (event.getCurrentItem().getItemMeta().getDisplayName().equalsIgnoreCase(plugin.message.Pig))
	    	        {
	    				if(!p.hasPermission("MyOtherWorldSpawners.free.pig")) 
	    				{
		    				EconomyResponse r = plugin.economy.withdrawPlayer(p, plugin.config.PricePig);
		    				if (r.transactionSuccess())
		    				{
			    				CreatureSpawner s = (CreatureSpawner) b.getState();
			    				s.setSpawnedType(EntityType.PIG);
			    				s.update();
			    				p.closeInventory();
			    				p.sendMessage(plugin.message.Prefix + plugin.message.SpawnerSet + EntityType.PIG );   
		    				}
			   				else 
			    			{
			    				p.closeInventory();
			    				p.sendMessage(plugin.message.Prefix + plugin.message.Money);
			    			}
	    				}
	    				else
	    				{
		    				CreatureSpawner s = (CreatureSpawner) b.getState();
		    				s.setSpawnedType(EntityType.PIG);
		    				s.update();
		    				p.closeInventory();
		    				p.sendMessage(plugin.message.Prefix + plugin.message.SpawnerSet + EntityType.PIG );  			
	    				}
	    	        } 
	    			if (event.getCurrentItem().getItemMeta().getDisplayName().equalsIgnoreCase(plugin.message.PigZombie))
	    	        {
	    				if(!p.hasPermission("MyOtherWorldSpawners.free.pigzombie")) 
	    				{
		    				EconomyResponse r = plugin.economy.withdrawPlayer(p, plugin.config.PricePigZombie);
		    				if (r.transactionSuccess())
		    				{
			    				CreatureSpawner s = (CreatureSpawner) b.getState();
			    				s.setSpawnedType(EntityType.PIG_ZOMBIE);
			    				s.update();
			    				p.closeInventory();
			    				p.sendMessage(plugin.message.Prefix + plugin.message.SpawnerSet + EntityType.PIG_ZOMBIE );   
		    				}
			   				else 
			    			{
			    				p.closeInventory();
			    				p.sendMessage(plugin.message.Prefix + plugin.message.Money);
			    			}
	    				}
	    				else
	    				{
		    				CreatureSpawner s = (CreatureSpawner) b.getState();
		    				s.setSpawnedType(EntityType.PIG_ZOMBIE);
		    				s.update();
		    				p.closeInventory();
		    				p.sendMessage(plugin.message.Prefix + plugin.message.SpawnerSet + EntityType.PIG_ZOMBIE );  			
	    				}
	    	        } 
	    			if (event.getCurrentItem().getItemMeta().getDisplayName().equalsIgnoreCase(plugin.message.Rabbit))
	    	        {	
	    				if(plugin.mode1_8)
	    				{
		    				if(!p.hasPermission("MyOtherWorldSpawners.free.rabbit")) 
		    				{
			    				EconomyResponse r = plugin.economy.withdrawPlayer(p, plugin.config.PriceRabbit);
			    				if (r.transactionSuccess())
			    				{
				    				CreatureSpawner s = (CreatureSpawner) b.getState();
				    				s.setSpawnedType(EntityType.RABBIT);
				    				s.update();
				    				p.closeInventory();
				    				p.sendMessage(plugin.message.Prefix + plugin.message.SpawnerSet + EntityType.RABBIT );   
			    				}
				   				else 
				    			{
				    				p.closeInventory();
				    				p.sendMessage(plugin.message.Prefix + plugin.message.Money);
				    			}
		    				}
		    				else
		    				{
			    				CreatureSpawner s = (CreatureSpawner) b.getState();
			    				s.setSpawnedType(EntityType.RABBIT);
			    				s.update();
			    				p.closeInventory();
			    				p.sendMessage(plugin.message.Prefix + plugin.message.SpawnerSet + EntityType.RABBIT );  			
		    				}
	    				}
    			    	else
    			    	{
    			    		p.closeInventory();
    			    		p.sendMessage("Only Spigot 1.8");   			    		
    			    	}
	    	        } 
	    			if (event.getCurrentItem().getItemMeta().getDisplayName().equalsIgnoreCase(plugin.message.Sheep))
	    	        {	
	    				if(!p.hasPermission("MyOtherWorldSpawners.free.sheep")) 
	    				{
		    				EconomyResponse r = plugin.economy.withdrawPlayer(p, plugin.config.PriceSheep);
		    				if (r.transactionSuccess())
		    				{
			    				CreatureSpawner s = (CreatureSpawner) b.getState();
			    				s.setSpawnedType(EntityType.SHEEP);
			    				s.update();
			    				p.closeInventory();
			    				p.sendMessage(plugin.message.Prefix + plugin.message.SpawnerSet + EntityType.SHEEP); 
		    				}
			   				else 
			    			{
			    				p.closeInventory();
			    				p.sendMessage(plugin.message.Prefix + plugin.message.Money);
			    			} 
	    				}
	    				else
	    				{
		    				CreatureSpawner s = (CreatureSpawner) b.getState();
		    				s.setSpawnedType(EntityType.SHEEP);
		    				s.update();
		    				p.closeInventory();
		    				p.sendMessage(plugin.message.Prefix + plugin.message.SpawnerSet + EntityType.SHEEP );  			
	    				}
	    	        } 
	    			if (event.getCurrentItem().getItemMeta().getDisplayName().equalsIgnoreCase(plugin.message.SilverFish))
	    	        {	
	    				if(!p.hasPermission("MyOtherWorldSpawners.free.silverfish")) 
	    				{
		    				EconomyResponse r = plugin.economy.withdrawPlayer(p, plugin.config.PriceSilverFish);
		    				if (r.transactionSuccess())
		    				{
			    				CreatureSpawner s = (CreatureSpawner) b.getState();
			    				s.setSpawnedType(EntityType.SILVERFISH);
			    				s.update();
			    				p.closeInventory();
			    				p.sendMessage(plugin.message.Prefix + plugin.message.SpawnerSet + EntityType.SILVERFISH); 
		    				}
			   				else 
			    			{
			    				p.closeInventory();
			    				p.sendMessage(plugin.message.Prefix + plugin.message.Money);
			    			}
	    				}
	    				else
	    				{
		    				CreatureSpawner s = (CreatureSpawner) b.getState();
		    				s.setSpawnedType(EntityType.SILVERFISH);
		    				s.update();
		    				p.closeInventory();
		    				p.sendMessage(plugin.message.Prefix + plugin.message.SpawnerSet + EntityType.SILVERFISH );  			
	    				}
	    	        } 
	    			if (event.getCurrentItem().getItemMeta().getDisplayName().equalsIgnoreCase(plugin.message.Skeleton))
	    	        {	
	    				if(!p.hasPermission("MyOtherWorldSpawners.free.skeleton")) 
	    				{
		    				EconomyResponse r = plugin.economy.withdrawPlayer(p, plugin.config.PriceSkeleton);
		    				if (r.transactionSuccess())
		    				{
			    				CreatureSpawner s = (CreatureSpawner) b.getState();
			    				s.setSpawnedType(EntityType.SKELETON);
			    				s.update();
			    				p.closeInventory();
			    				p.sendMessage(plugin.message.Prefix + plugin.message.SpawnerSet + EntityType.SKELETON); 
		    				}
			   				else 
			    			{
			    				p.closeInventory();
			    				p.sendMessage(plugin.message.Prefix + plugin.message.Money);
			    			}
	    				}
	    				else
	    				{
		    				CreatureSpawner s = (CreatureSpawner) b.getState();
		    				s.setSpawnedType(EntityType.SKELETON);
		    				s.update();
		    				p.closeInventory();
		    				p.sendMessage(plugin.message.Prefix + plugin.message.SpawnerSet + EntityType.SKELETON );  			
	    				}
	    	        } 
	    			if (event.getCurrentItem().getItemMeta().getDisplayName().equalsIgnoreCase(plugin.message.Slime))
	    	        {	
	    				if(!p.hasPermission("MyOtherWorldSpawners.free.slime")) 
	    				{
		    				EconomyResponse r = plugin.economy.withdrawPlayer(p, plugin.config.PriceSlime);
		    				if (r.transactionSuccess())
		    				{
			    				CreatureSpawner s = (CreatureSpawner) b.getState();
			    				s.setSpawnedType(EntityType.SLIME);
			    				s.update();
			    				p.closeInventory();
			    				p.sendMessage(plugin.message.Prefix + plugin.message.SpawnerSet + EntityType.SLIME); 
		    				}
			   				else 
			    			{
			    				p.closeInventory();
			    				p.sendMessage(plugin.message.Prefix + plugin.message.Money);
			    			} 
	    				}
	    				else
	    				{
		    				CreatureSpawner s = (CreatureSpawner) b.getState();
		    				s.setSpawnedType(EntityType.SLIME);
		    				s.update();
		    				p.closeInventory();
		    				p.sendMessage(plugin.message.Prefix + plugin.message.SpawnerSet + EntityType.SLIME );  			
	    				}
	    	        } 
	    			if (event.getCurrentItem().getItemMeta().getDisplayName().equalsIgnoreCase(plugin.message.SnowMan))
	    	        {	
	    				if(!p.hasPermission("MyOtherWorldSpawners.free.snowman")) 
	    				{
		    				EconomyResponse r = plugin.economy.withdrawPlayer(p, plugin.config.PriceSnowMan);
		    				if (r.transactionSuccess())
		    				{
			    				CreatureSpawner s = (CreatureSpawner) b.getState();
			    				s.setSpawnedType(EntityType.SNOWMAN);
			    				s.update();
			    				p.closeInventory();
			    				p.sendMessage(plugin.message.Prefix + plugin.message.SpawnerSet + EntityType.SNOWMAN); 
		    				}
			   				else 
			    			{
			    				p.closeInventory();
			    				p.sendMessage(plugin.message.Prefix + plugin.message.Money);
			    			}
	    				}
	    				else
	    				{
		    				CreatureSpawner s = (CreatureSpawner) b.getState();
		    				s.setSpawnedType(EntityType.SNOWMAN);
		    				s.update();
		    				p.closeInventory();
		    				p.sendMessage(plugin.message.Prefix + plugin.message.SpawnerSet + EntityType.SNOWMAN );  			
	    				}
	    	        } 
	    			if (event.getCurrentItem().getItemMeta().getDisplayName().equalsIgnoreCase(plugin.message.Spider))
	    	        {
	    				if(!p.hasPermission("MyOtherWorldSpawners.free.spider")) 
	    				{	    				
		    				EconomyResponse r = plugin.economy.withdrawPlayer(p, plugin.config.PriceSpider);
		    				if (r.transactionSuccess())
		    				{
			    				CreatureSpawner s = (CreatureSpawner) b.getState();
			    				s.setSpawnedType(EntityType.SPIDER);
			    				s.update();
			    				p.closeInventory();
			    				p.sendMessage(plugin.message.Prefix + plugin.message.SpawnerSet + EntityType.SPIDER); 
		    				}
			   				else 
			    			{
			    				p.closeInventory();
			    				p.sendMessage(plugin.message.Prefix + plugin.message.Money);
			    			}
	    				}
	    				else
	    				{
		    				CreatureSpawner s = (CreatureSpawner) b.getState();
		    				s.setSpawnedType(EntityType.SPIDER);
		    				s.update();
		    				p.closeInventory();
		    				p.sendMessage(plugin.message.Prefix + plugin.message.SpawnerSet + EntityType.SPIDER );  			
	    				}	
	    	        } 
	    			if (event.getCurrentItem().getItemMeta().getDisplayName().equalsIgnoreCase(plugin.message.Squid))
	    	        {
	    				if(!p.hasPermission("MyOtherWorldSpawners.free.squid")) 
	    				{
		    				EconomyResponse r = plugin.economy.withdrawPlayer(p, plugin.config.PriceSquid);
		    				if (r.transactionSuccess())
		    				{
			    				CreatureSpawner s = (CreatureSpawner) b.getState();
			    				s.setSpawnedType(EntityType.SQUID);
			    				s.update();
			    				p.closeInventory();
			    				p.sendMessage(plugin.message.Prefix + plugin.message.SpawnerSet + EntityType.SQUID); 
		    				}
			   				else 
			    			{
			    				p.closeInventory();
			    				p.sendMessage(plugin.message.Prefix + plugin.message.Money);
			    			}
	    				}
	    				else
	    				{
		    				CreatureSpawner s = (CreatureSpawner) b.getState();
		    				s.setSpawnedType(EntityType.SQUID);
		    				s.update();
		    				p.closeInventory();
		    				p.sendMessage(plugin.message.Prefix + plugin.message.SpawnerSet + EntityType.SQUID );  			
	    				}	
	    	        } 
	    			if (event.getCurrentItem().getItemMeta().getDisplayName().equalsIgnoreCase(plugin.message.Villager))
	    	        {	
	    				if(!p.hasPermission("MyOtherWorldSpawners.free.villager")) 
	    				{
		    				EconomyResponse r = plugin.economy.withdrawPlayer(p, plugin.config.PriceVillager);
		    				if (r.transactionSuccess())
		    				{
			    				CreatureSpawner s = (CreatureSpawner) b.getState();
			    				s.setSpawnedType(EntityType.VILLAGER);
			    				s.update();
			    				p.closeInventory();
			    				p.sendMessage(plugin.message.Prefix + plugin.message.SpawnerSet + EntityType.VILLAGER); 
		    				}
			   				else 
			    			{
			    				p.closeInventory();
			    				p.sendMessage(plugin.message.Prefix + plugin.message.Money);
			    			}
	    				}
	    				else
	    				{
		    				CreatureSpawner s = (CreatureSpawner) b.getState();
		    				s.setSpawnedType(EntityType.VILLAGER);
		    				s.update();
		    				p.closeInventory();
		    				p.sendMessage(plugin.message.Prefix + plugin.message.SpawnerSet + EntityType.VILLAGER );  			
	    				}
	    	        } 
	    			if (event.getCurrentItem().getItemMeta().getDisplayName().equalsIgnoreCase(plugin.message.Witch))
	    	        {	
	    				if(!p.hasPermission("MyOtherWorldSpawners.free.witch")) 
	    				{
		    				EconomyResponse r = plugin.economy.withdrawPlayer(p, plugin.config.PriceWitch);
		    				if (r.transactionSuccess())
		    				{
			    				CreatureSpawner s = (CreatureSpawner) b.getState();
			    				s.setSpawnedType(EntityType.WITCH);
			    				s.update();
			    				p.closeInventory();
			    				p.sendMessage(plugin.message.Prefix + plugin.message.SpawnerSet + EntityType.WITCH); 
		    				}
			   				else 
			    			{
			    				p.closeInventory();
			    				p.sendMessage(plugin.message.Prefix + plugin.message.Money);
			    			}
	    				}
	    				else
	    				{
		    				CreatureSpawner s = (CreatureSpawner) b.getState();
		    				s.setSpawnedType(EntityType.WITCH);
		    				s.update();
		    				p.closeInventory();
		    				p.sendMessage(plugin.message.Prefix + plugin.message.SpawnerSet + EntityType.WITCH );  			
	    				}
	    	        }
	    			if (event.getCurrentItem().getItemMeta().getDisplayName().equalsIgnoreCase(plugin.message.Wither))
	    	        {	
	    				if(!p.hasPermission("MyOtherWorldSpawners.free.witcher")) 
	    				{
		    				EconomyResponse r = plugin.economy.withdrawPlayer(p, plugin.config.PriceWither);
		    				if (r.transactionSuccess())
		    				{
			    				CreatureSpawner s = (CreatureSpawner) b.getState();
			    				s.setSpawnedType(EntityType.WITHER);
			    				s.update();
			    				p.closeInventory();
			    				p.sendMessage(plugin.message.Prefix + plugin.message.SpawnerSet + EntityType.WITHER); 
		    				}
			   				else 
			    			{
			    				p.closeInventory();
			    				p.sendMessage(plugin.message.Prefix + plugin.message.Money);
			    			}
	    				}
	    				else
	    				{
		    				CreatureSpawner s = (CreatureSpawner) b.getState();
		    				s.setSpawnedType(EntityType.WITHER);
		    				s.update();
		    				p.closeInventory();
		    				p.sendMessage(plugin.message.Prefix + plugin.message.SpawnerSet + EntityType.WITHER );  			
	    				}
	    	        }
	    			if (event.getCurrentItem().getItemMeta().getDisplayName().equalsIgnoreCase(plugin.message.Wolf))
	    	        {	
	    				if(!p.hasPermission("MyOtherWorldSpawners.free.wolf")) 
	    				{
		    				EconomyResponse r = plugin.economy.withdrawPlayer(p, plugin.config.PriceWolf);
		    				if (r.transactionSuccess())
		    				{
			    				CreatureSpawner s = (CreatureSpawner) b.getState();
			    				s.setSpawnedType(EntityType.WOLF);
			    				s.update();
			    				p.closeInventory();
			    				p.sendMessage(plugin.message.Prefix + plugin.message.SpawnerSet + EntityType.WOLF); 
		    				}
			   				else 
			    			{
			    				p.closeInventory();
			    				p.sendMessage(plugin.message.Prefix + plugin.message.Money);
			    			}
	    				}
	    				else
	    				{
		    				CreatureSpawner s = (CreatureSpawner) b.getState();
		    				s.setSpawnedType(EntityType.WOLF);
		    				s.update();
		    				p.closeInventory();
		    				p.sendMessage(plugin.message.Prefix + plugin.message.SpawnerSet + EntityType.WOLF );  			
	    				}
	    	        }
	    			if (event.getCurrentItem().getItemMeta().getDisplayName().equalsIgnoreCase(plugin.message.Zombie))
	    	        {
	    				if(!p.hasPermission("MyOtherWorldSpawners.free.zombie")) 
	    				{
		    				EconomyResponse r = plugin.economy.withdrawPlayer(p, plugin.config.PriceZombie);
		    				if (r.transactionSuccess())
		    				{
			    				CreatureSpawner s = (CreatureSpawner) b.getState();
			    				s.setSpawnedType(EntityType.ZOMBIE);
			    				s.update();
			    				p.closeInventory();
			    				p.sendMessage(plugin.message.Prefix + plugin.message.SpawnerSet + EntityType.ZOMBIE); 
		    				}
			   				else 
			    			{
			    				p.closeInventory();
			    				p.sendMessage(plugin.message.Prefix + plugin.message.Money);
			    			}
	    				}
	    				else
	    				{
		    				CreatureSpawner s = (CreatureSpawner) b.getState();
		    				s.setSpawnedType(EntityType.ZOMBIE);
		    				s.update();
		    				p.closeInventory();
		    				p.sendMessage(plugin.message.Prefix + plugin.message.SpawnerSet + EntityType.ZOMBIE );  			
	    				}
	    	        }
    			}
    		}
    	      catch (Exception localException) {}
    	}
	}
}
