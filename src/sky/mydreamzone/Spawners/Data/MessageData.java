package net.myotherworld.Spawners.Data;

import java.util.List;

import org.bukkit.ChatColor;
import org.bukkit.configuration.file.YamlConfiguration;

public class MessageData 
{
	public String Permissions;
	public String Prefix;
	public String SpawnerLook;
	public String SpanwerWrong;
	public String SpawnerSet;
	public String Money;
	public String Price;
	public String NoCommands;
	public List<String> Admins;
	
	public String Menu;
	public String Bat;
	public String Blaze;
	public String CaveSpider;
	public String Chicken;
	public String Cow;
	public String Crepper;
	public String EnderDragon;
	public String Enderman;
	public String Ghast;
	public String Giant;
	public String Guardian;
	public String IronGolem;
	public String MagmaCube;
	public String MushroomCow;
	public String Ocelot;
	public String Pig;
	public String PigZombie;
	public String Rabbit;
	public String Sheep;
	public String SilverFish;
	public String Skeleton;
	public String Slime;
	public String SnowMan;
	public String Spider;
	public String Squid;
	public String Villager;
	public String Witch;
	public String Wither;
	public String Wolf;
	public String Zombie;
	
	
	public MessageData(YamlConfiguration message) 
	{
		Prefix = ChatColor.translateAlternateColorCodes('&',message.getString("Prefix", "Spawners"));	
		
		Permissions = ChatColor.translateAlternateColorCodes('&',message.getString("Message.Permissions", "You do not have permission!" ));
			
		SpawnerLook = ChatColor.translateAlternateColorCodes('&',message.getString("Message.SpawnerLook" ,"Musisz patrzec na spawner"));
		SpanwerWrong = ChatColor.translateAlternateColorCodes('&',message.getString("Message.SpanwerWrong" ,"To nie jest Spawner"));
		SpawnerSet = ChatColor.translateAlternateColorCodes('&',message.getString("Message.SpawnerSet" ,"Spawner ustawiono na:"));
		Money = ChatColor.translateAlternateColorCodes('&',message.getString("Message.Money" ,"Nie masz pieniedzy na zmiane"));
		Price = ChatColor.translateAlternateColorCodes('&',message.getString("Message.Price" ,"Cena"));
		NoCommands = ChatColor.translateAlternateColorCodes('&',message.getString("Message.NoCommands", "Brak komendy wpisz /MowPing" ));
		
		Admins = message.getStringList("Admins");
		
		Menu = ChatColor.translateAlternateColorCodes('&',message.getString("Name.Menu", "Mobs" ));
		Bat = ChatColor.translateAlternateColorCodes('&',message.getString("Name.Bat", "Bat" ));
		Blaze = ChatColor.translateAlternateColorCodes('&',message.getString("Name.Blaze", "Blaze" ));
		CaveSpider = ChatColor.translateAlternateColorCodes('&',message.getString("Name.CaveSpider", "CaveSpider" ));
		Chicken = ChatColor.translateAlternateColorCodes('&',message.getString("Name.Chicken", "Chicken" ));
		Cow = ChatColor.translateAlternateColorCodes('&',message.getString("Name.Cow", "Cow" ));
		Crepper = ChatColor.translateAlternateColorCodes('&',message.getString("Name.Crepper", "Crepper" ));
		EnderDragon = ChatColor.translateAlternateColorCodes('&',message.getString("Name.EnderDragon", "EnderDragon" ));
		Enderman = ChatColor.translateAlternateColorCodes('&',message.getString("Name.Enderman", "Enderman" ));
		Ghast = ChatColor.translateAlternateColorCodes('&',message.getString("Name.Ghast", "Ghast" ));
		Giant = ChatColor.translateAlternateColorCodes('&',message.getString("Name.Giant", "Giant" ));
		Guardian = ChatColor.translateAlternateColorCodes('&',message.getString("Name.Guardian", "Guardian" ));
		IronGolem = ChatColor.translateAlternateColorCodes('&',message.getString("Name.IronGolem", "IronGolem" ));
		MagmaCube = ChatColor.translateAlternateColorCodes('&',message.getString("Name.MagmaCube", "MagmaCube" ));
		MushroomCow = ChatColor.translateAlternateColorCodes('&',message.getString("Name.MushroomCow", "MushroomCow" ));
		Ocelot = ChatColor.translateAlternateColorCodes('&',message.getString("Name.Ocelot", "Ocelot" ));
		Pig = ChatColor.translateAlternateColorCodes('&',message.getString("Name.Pig", "Pig" ));
		PigZombie = ChatColor.translateAlternateColorCodes('&',message.getString("Name.PigZombie", "PigZombie" ));
		Rabbit = ChatColor.translateAlternateColorCodes('&',message.getString("Name.Rabbit", "Rabbit" ));
		Sheep = ChatColor.translateAlternateColorCodes('&',message.getString("Name.Sheep", "Sheep" ));
		SilverFish = ChatColor.translateAlternateColorCodes('&',message.getString("Name.SilverFish", "SilverFish" ));
		Skeleton = ChatColor.translateAlternateColorCodes('&',message.getString("Name.Skeleton", "Skeleton" ));
		Slime = ChatColor.translateAlternateColorCodes('&',message.getString("Name.Slime", "Slime" ));
		SnowMan = ChatColor.translateAlternateColorCodes('&',message.getString("Name.SnowMan", "SnowMan" ));
		Spider = ChatColor.translateAlternateColorCodes('&',message.getString("Name.Spider", "Spider" ));
		Squid = ChatColor.translateAlternateColorCodes('&',message.getString("Name.Squid", "Squid" ));
		Villager = ChatColor.translateAlternateColorCodes('&',message.getString("Name.Villager", "Villager" ));
		Witch = ChatColor.translateAlternateColorCodes('&',message.getString("Name.Witch", "Witch" ));
		Wither = ChatColor.translateAlternateColorCodes('&',message.getString("Name.Wither", "Wither" ));
		Wolf = ChatColor.translateAlternateColorCodes('&',message.getString("Name.Wolf", "Wolf" ));
		Zombie = ChatColor.translateAlternateColorCodes('&',message.getString("Name.Zombie", "Zombie" ));
		
	}
}
