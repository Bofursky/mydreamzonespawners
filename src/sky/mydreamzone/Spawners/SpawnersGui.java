package net.myotherworld.Spawners;

import java.util.ArrayList;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.inventory.meta.SkullMeta;

public class SpawnersGui 
{
	private Spawners plugin;
	
	public SpawnersGui(Spawners plugin)
	{
		this.plugin = plugin;
	}
	@SuppressWarnings("deprecation")
	public void SpawnerGuiOpen(Player p) 
	{
		Inventory inv = Bukkit.createInventory(null, 36, plugin.message.Menu);
	
	    p.playSound(p.getLocation(), Sound.FIREWORK_BLAST, 100.0F, 100.0F);
	    
	    if(p.hasPermission("MyOtherWorldSpawners.bat") || (p.hasPermission("MyOtherWorldSpawners.free.bat")))
	    {
	    	ItemStack IS = new ItemStack(383, 1, (short) 65);
		    ItemMeta ISMeta = IS.getItemMeta();
		    ISMeta.setDisplayName(plugin.message.Bat);
		    ArrayList<String> lore = new ArrayList<String>();
		    if(!p.hasPermission("MyOtherWorldSpawners.free.bat"))
		    {
		    	lore.add(plugin.message.Price+ plugin.config.PriceBat);
		    }
		    ISMeta.setLore(lore);
		    IS.setItemMeta(ISMeta);
		    inv.addItem(IS);
	    }
	    if(p.hasPermission("MyOtherWorldSpawners.blaze") || (p.hasPermission("MyOtherWorldSpawners.free.blaze")))
	    {
	    	ItemStack IS = new ItemStack(383, 1, (short) 61);
		    ItemMeta ISMeta = IS.getItemMeta();
		    ISMeta.setDisplayName(plugin.message.Blaze);
		    ArrayList<String> lore = new ArrayList<String>();
		    lore.add(plugin.message.Price+ plugin.config.PriceBlaze);
		    ISMeta.setLore(lore);
		    IS.setItemMeta(ISMeta);
		    inv.addItem(IS);
	    }
	    if(p.hasPermission("MyOtherWorldSpawners.cavespider") || (p.hasPermission("MyOtherWorldSpawners.free.cavespider")))
	    {
	    	ItemStack IS = new ItemStack(383, 1, (short) 59);
		    ItemMeta ISMeta = IS.getItemMeta();
		    ISMeta.setDisplayName(plugin.message.CaveSpider);
		    ArrayList<String> lore = new ArrayList<String>();
		    lore.add(plugin.message.Price+ plugin.config.PriceCaveSpider);
		    ISMeta.setLore(lore);
		    IS.setItemMeta(ISMeta);
		    inv.addItem(IS);
	    }
	    if(p.hasPermission("MyOtherWorldSpawners.chickien") || (p.hasPermission("MyOtherWorldSpawners.free.chickien")))
	    {
	    	ItemStack IS = new ItemStack(383, 1, (short) 93);
		    ItemMeta ISMeta = IS.getItemMeta();
		    ISMeta.setDisplayName(plugin.message.Chicken);
		    ArrayList<String> lore = new ArrayList<String>();
		    lore.add(plugin.message.Price+ plugin.config.PriceChicken);
		    ISMeta.setLore(lore);
		    IS.setItemMeta(ISMeta);
		    inv.addItem(IS);
	    }
	    if(p.hasPermission("MyOtherWorldSpawners.cow") || (p.hasPermission("MyOtherWorldSpawners.free.cow")))
	    {
	    	ItemStack IS = new ItemStack(383, 1, (short) 92);
		    ItemMeta ISMeta = IS.getItemMeta();
		    ISMeta.setDisplayName(plugin.message.Cow);
		    ArrayList<String> lore = new ArrayList<String>();
		    lore.add(plugin.message.Price+ plugin.config.PriceCow);
		    ISMeta.setLore(lore);
		    IS.setItemMeta(ISMeta);
		    inv.addItem(IS);
	    }
	    if(p.hasPermission("MyOtherWorldSpawners.crepper") || (p.hasPermission("MyOtherWorldSpawners.free.crepper")))
	    {
	    	ItemStack IS = new ItemStack(383, 1, (short) 50);
		    ItemMeta ISMeta = IS.getItemMeta();
		    ISMeta.setDisplayName(plugin.message.Crepper);
		    ArrayList<String> lore = new ArrayList<String>();
		    lore.add(plugin.message.Price+ plugin.config.PriceCrepper);
		    ISMeta.setLore(lore);
		    IS.setItemMeta(ISMeta);
		    inv.addItem(IS);
	    }
	    if(p.hasPermission("MyOtherWorldSpawners.enderdragon") || (p.hasPermission("MyOtherWorldSpawners.free.enderdragon")))
	    {
	    	ItemStack IS = new ItemStack(383, 1, (short) 63);
		    ItemMeta ISMeta = IS.getItemMeta();
		    ISMeta.setDisplayName(plugin.message.EnderDragon);
		    ArrayList<String> lore = new ArrayList<String>();
		    lore.add(plugin.message.Price+ plugin.config.PriceEnderDragon);
		    ISMeta.setLore(lore);
		    IS.setItemMeta(ISMeta);
		    inv.addItem(IS);
	    }
	    if(p.hasPermission("MyOtherWorldSpawners.enderman") || (p.hasPermission("MyOtherWorldSpawners.free.enderman")))
	    {
	    	ItemStack IS = new ItemStack(383, 1, (short) 58);
		    ItemMeta ISMeta = IS.getItemMeta();
		    ISMeta.setDisplayName(plugin.message.Enderman);
		    ArrayList<String> lore = new ArrayList<String>();
		    lore.add(plugin.message.Price+ plugin.config.PriceEnderman);
		    ISMeta.setLore(lore);
		    IS.setItemMeta(ISMeta);
		    inv.addItem(IS);
	    }
	    if(p.hasPermission("MyOtherWorldSpawners.ghast") || (p.hasPermission("MyOtherWorldSpawners.free.ghast")))
	    {
	    	ItemStack IS = new ItemStack(383, 1, (short) 56);
		    ItemMeta ISMeta = IS.getItemMeta();
		    ISMeta.setDisplayName(plugin.message.Ghast);
		    ArrayList<String> lore = new ArrayList<String>();
		    lore.add(plugin.message.Price+ plugin.config.PriceGhast);
		    ISMeta.setLore(lore);
		    IS.setItemMeta(ISMeta);
		    inv.addItem(IS);
	    }
	    if(p.hasPermission("MyOtherWorldSpawners.giant") || (p.hasPermission("MyOtherWorldSpawners.free.giant")))
	    {
	    	ItemStack IS = new ItemStack(383, 1, (short) 53);
		    ItemMeta ISMeta = IS.getItemMeta();
		    ISMeta.setDisplayName(plugin.message.Giant);
		    ArrayList<String> lore = new ArrayList<String>();
		    lore.add(plugin.message.Price+ plugin.config.PriceGiant);
		    ISMeta.setLore(lore);
		    IS.setItemMeta(ISMeta);
		    inv.addItem(IS);
	    }
	    if(p.hasPermission("MyOtherWorldSpawners.guardian") || (p.hasPermission("MyOtherWorldSpawners.free.guardian")))
	    {
	    	ItemStack IS = new ItemStack(383, 1, (short) 68);
		    ItemMeta ISMeta = IS.getItemMeta();
		    ISMeta.setDisplayName(plugin.message.Guardian);
		    ArrayList<String> lore = new ArrayList<String>();
		    lore.add(plugin.message.Price+ plugin.config.PriceGuardian);
		    ISMeta.setLore(lore);
		    IS.setItemMeta(ISMeta);
		    inv.addItem(IS);
	    }	   
	    if(p.hasPermission("MyOtherWorldSpawners.irongolem") || (p.hasPermission("MyOtherWorldSpawners.free.irongolem")))
	    {
	    	ItemStack IS = new ItemStack(383, 1, (short) 99);
		    ItemMeta ISMeta = IS.getItemMeta();
		    ISMeta.setDisplayName(plugin.message.IronGolem);
		    ArrayList<String> lore = new ArrayList<String>();
		    lore.add(plugin.message.Price+ plugin.config.PriceIronGolem);
		    ISMeta.setLore(lore);
		    IS.setItemMeta(ISMeta);
		    inv.addItem(IS);
	    }	
	    if(p.hasPermission("MyOtherWorldSpawners.magmacube") || (p.hasPermission("MyOtherWorldSpawners.free.magmacube")))
	    {
	    	ItemStack IS = new ItemStack(383, 1, (short) 62);
		    ItemMeta ISMeta = IS.getItemMeta();
		    ISMeta.setDisplayName(plugin.message.MagmaCube);
		    ArrayList<String> lore = new ArrayList<String>();
		    lore.add(plugin.message.Price+ plugin.config.PriceMagmaCube);
		    ISMeta.setLore(lore);
		    IS.setItemMeta(ISMeta);
		    inv.addItem(IS);
	    }
	    if(p.hasPermission("MyOtherWorldSpawners.mushroomcow") || (p.hasPermission("MyOtherWorldSpawners.free.mushroomcow")))
	    {
	    	ItemStack IS = new ItemStack(383, 1, (short) 96);
		    ItemMeta ISMeta = IS.getItemMeta();
		    ISMeta.setDisplayName(plugin.message.MushroomCow);
		    ArrayList<String> lore = new ArrayList<String>();
		    lore.add(plugin.message.Price+ plugin.config.PriceMushroomCow);
		    ISMeta.setLore(lore);
		    IS.setItemMeta(ISMeta);
		    inv.addItem(IS);
	    }
	    if(p.hasPermission("MyOtherWorldSpawners.ocelot") || (p.hasPermission("MyOtherWorldSpawners.free.ocelot")))
	    {
	    	ItemStack IS = new ItemStack(383, 1, (short) 98);
		    ItemMeta ISMeta = IS.getItemMeta();
		    ISMeta.setDisplayName(plugin.message.Ocelot);
		    ArrayList<String> lore = new ArrayList<String>();
		    lore.add(plugin.message.Price+ plugin.config.PriceOcelot);
		    ISMeta.setLore(lore);
		    IS.setItemMeta(ISMeta);
		    inv.addItem(IS);
	    }
	    if(p.hasPermission("MyOtherWorldSpawners.pig") || (p.hasPermission("MyOtherWorldSpawners.free.pig")))
	    {
	    	ItemStack IS = new ItemStack(383, 1, (short) 90);
		    ItemMeta ISMeta = IS.getItemMeta();
		    ISMeta.setDisplayName(plugin.message.Pig);
		    ArrayList<String> lore = new ArrayList<String>();
		    lore.add(plugin.message.Price+ plugin.config.PricePig);
		    ISMeta.setLore(lore);
		    IS.setItemMeta(ISMeta);
		    inv.addItem(IS);
	    }
	    if(p.hasPermission("MyOtherWorldSpawners.pigzombie") || (p.hasPermission("MyOtherWorldSpawners.free.pigzombie")))
	    {
	    	ItemStack IS = new ItemStack(383, 1, (short) 57);
		    ItemMeta ISMeta = IS.getItemMeta();
		    ISMeta.setDisplayName(plugin.message.PigZombie);
		    ArrayList<String> lore = new ArrayList<String>();
		    lore.add(plugin.message.Price+ plugin.config.PricePigZombie);
		    ISMeta.setLore(lore);
		    IS.setItemMeta(ISMeta);
		    inv.addItem(IS);
	    }
	    if(p.hasPermission("MyOtherWorldSpawners.rabbit") || (p.hasPermission("MyOtherWorldSpawners.free.rabbit")))
	    {
	    	ItemStack IS = new ItemStack(383, 1, (short) 101);
		    ItemMeta ISMeta = IS.getItemMeta();
		    ISMeta.setDisplayName(plugin.message.Rabbit);
		    ArrayList<String> lore = new ArrayList<String>();
		    lore.add(plugin.message.Price+ plugin.config.PriceRabbit);
		    ISMeta.setLore(lore);
		    IS.setItemMeta(ISMeta);
		    inv.addItem(IS);
	    }
	    if(p.hasPermission("MyOtherWorldSpawners.sheep") || (p.hasPermission("MyOtherWorldSpawners.free.sheep")))
	    {
	    	ItemStack IS = new ItemStack(383, 1, (short) 91);
		    ItemMeta ISMeta = IS.getItemMeta();
		    ISMeta.setDisplayName(plugin.message.Sheep);
		    ArrayList<String> lore = new ArrayList<String>();
		    lore.add(plugin.message.Price+ plugin.config.PriceSheep);
		    ISMeta.setLore(lore);
		    IS.setItemMeta(ISMeta);
		    inv.addItem(IS);
	    }
	    if(p.hasPermission("MyOtherWorldSpawners.silverfish") || (p.hasPermission("MyOtherWorldSpawners.free.silverfish")))
	    {
	    	ItemStack IS = new ItemStack(383, 1, (short) 60);
		    ItemMeta ISMeta = IS.getItemMeta();
		    ISMeta.setDisplayName(plugin.message.SilverFish);
		    ArrayList<String> lore = new ArrayList<String>();
		    lore.add(plugin.message.Price+ plugin.config.PriceSilverFish);
		    ISMeta.setLore(lore);
		    IS.setItemMeta(ISMeta);
		    inv.addItem(IS);
	    }
	    if(p.hasPermission("MyOtherWorldSpawners.skeleton") || (p.hasPermission("MyOtherWorldSpawners.free.skeleton")))
	    {
	    	ItemStack IS = new ItemStack(383, 1, (short) 51);
		    ItemMeta ISMeta = IS.getItemMeta();
		    ISMeta.setDisplayName(plugin.message.Skeleton);
		    ArrayList<String> lore = new ArrayList<String>();
		    lore.add(plugin.message.Price+ plugin.config.PriceSkeleton);
		    ISMeta.setLore(lore);
		    IS.setItemMeta(ISMeta);
		    inv.addItem(IS);
	    }
	    if(p.hasPermission("MyOtherWorldSpawners.slime") || (p.hasPermission("MyOtherWorldSpawners.free.slime")))
	    {
	    	ItemStack IS = new ItemStack(383, 1, (short) 55);
		    ItemMeta ISMeta = IS.getItemMeta();
		    ISMeta.setDisplayName(plugin.message.Slime);
		    ArrayList<String> lore = new ArrayList<String>();
		    lore.add(plugin.message.Price+ plugin.config.PriceSlime);
		    ISMeta.setLore(lore);
		    IS.setItemMeta(ISMeta);
		    inv.addItem(IS);
	    }
	    if(p.hasPermission("MyOtherWorldSpawners.snowman") || (p.hasPermission("MyOtherWorldSpawners.free.snowman")))
	    {
	    	ItemStack IS = new ItemStack(383, 1, (short) 97);
		    ItemMeta ISMeta = IS.getItemMeta();
		    ISMeta.setDisplayName(plugin.message.SnowMan);
		    ArrayList<String> lore = new ArrayList<String>();
		    lore.add(plugin.message.Price+ plugin.config.PriceSnowMan);
		    ISMeta.setLore(lore);
		    IS.setItemMeta(ISMeta);
		    inv.addItem(IS);
	    }
	    if(p.hasPermission("MyOtherWorldSpawners.spider") || (p.hasPermission("MyOtherWorldSpawners.free.spider")))
	    {
	    	ItemStack IS = new ItemStack(383, 1, (short) 52);
		    ItemMeta ISMeta = IS.getItemMeta();
		    ISMeta.setDisplayName(plugin.message.Spider);
		    ArrayList<String> lore = new ArrayList<String>();
		    lore.add(plugin.message.Price+ plugin.config.PriceSpider);
		    ISMeta.setLore(lore);
		    IS.setItemMeta(ISMeta);
		    inv.addItem(IS);
	    }
	    if(p.hasPermission("MyOtherWorldSpawners.squid") || (p.hasPermission("MyOtherWorldSpawners.free.squid")))
	    {
	    	ItemStack IS = new ItemStack(383, 1, (short) 94);
		    ItemMeta ISMeta = IS.getItemMeta();
		    ISMeta.setDisplayName(plugin.message.Squid);
		    ArrayList<String> lore = new ArrayList<String>();
		    lore.add(plugin.message.Price+ plugin.config.PriceSquid);
		    ISMeta.setLore(lore);
		    IS.setItemMeta(ISMeta);
		    inv.addItem(IS);
	    }  
	    if(p.hasPermission("MyOtherWorldSpawners.villager") || (p.hasPermission("MyOtherWorldSpawners.free.villager")))
	    {
	    	ItemStack IS = new ItemStack(383, 1, (short) 120);
		    ItemMeta ISMeta = IS.getItemMeta();
		    ISMeta.setDisplayName(plugin.message.Villager);
		    ArrayList<String> lore = new ArrayList<String>();
		    lore.add(plugin.message.Price+ plugin.config.PriceVillager);
		    ISMeta.setLore(lore);
		    IS.setItemMeta(ISMeta);
		    inv.addItem(IS);
	    }
	    if(p.hasPermission("MyOtherWorldSpawners.witch") || (p.hasPermission("MyOtherWorldSpawners.free.witch")))
	    {
	    	ItemStack IS = new ItemStack(383, 1, (short) 66);
		    ItemMeta ISMeta = IS.getItemMeta();
		    ISMeta.setDisplayName(plugin.message.Witch);
		    ArrayList<String> lore = new ArrayList<String>();
		    lore.add(plugin.message.Price+ plugin.config.PriceWitch);
		    ISMeta.setLore(lore);
		    IS.setItemMeta(ISMeta);
		    inv.addItem(IS);
	    } 
	    if(p.hasPermission("MyOtherWorldSpawners.wither") || (p.hasPermission("MyOtherWorldSpawners.free.wither")))
	    {
	    	ItemStack IS = new ItemStack(383, 1, (short) 38);
		    ItemMeta ISMeta = IS.getItemMeta();
		    ISMeta.setDisplayName(plugin.message.Wither);
		    ArrayList<String> lore = new ArrayList<String>();
		    lore.add(plugin.message.Price+ plugin.config.PriceWither);
		    ISMeta.setLore(lore);
		    IS.setItemMeta(ISMeta);
		    inv.addItem(IS);
	    } 
	    if(p.hasPermission("MyOtherWorldSpawners.wolf") || (p.hasPermission("MyOtherWorldSpawners.free.wolf")))
	    {
	    	ItemStack IS = new ItemStack(383, 1, (short) 95);
		    ItemMeta ISMeta = IS.getItemMeta();
		    ISMeta.setDisplayName(plugin.message.Wolf);
		    ArrayList<String> lore = new ArrayList<String>();
		    lore.add(plugin.message.Price+ plugin.config.PriceWolf);
		    ISMeta.setLore(lore);
		    IS.setItemMeta(ISMeta);
		    inv.addItem(IS);
	    } 
	    if(p.hasPermission("MyOtherWorldSpawners.zombie") || (p.hasPermission("MyOtherWorldSpawners.free.zombie")))
	    {
	    	ItemStack IS = new ItemStack(383, 1, (short) 54);
		    ItemMeta ISMeta = IS.getItemMeta();
		    ISMeta.setDisplayName(plugin.message.Zombie);
		    ArrayList<String> lore = new ArrayList<String>();
		    lore.add(plugin.message.Price+ plugin.config.PriceZombie);
		    ISMeta.setLore(lore);
		    IS.setItemMeta(ISMeta);
		    inv.addItem(IS);
	    } 
	    if(p.hasPermission("MyOtherWorldSpawners.spawner"))
	    {
	    	SkullMeta  meta = (SkullMeta) Bukkit.getItemFactory().getItemMeta(Material.SKULL_ITEM);
	    	meta.setOwner("Skynets");
	    	ItemStack stack = new ItemStack(Material.SKULL_ITEM,1 , (byte)3);
		    ArrayList<String> lore = new ArrayList<String>();
		    lore.add("Skynets");
		    lore.add("Free Version");
		    lore.add("MyOtherWorld.NET");
		    meta.setLore(lore);
	    	meta.setDisplayName("�6Autor Spawners");
	    	stack.setItemMeta(meta);	  
		    inv.addItem(stack);
	    }
	    p.openInventory(inv);
	}

}
