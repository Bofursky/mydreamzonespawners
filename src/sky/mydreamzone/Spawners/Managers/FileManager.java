package net.myotherworld.Spawners.Managers;

import java.io.File;

import net.myotherworld.Spawners.Spawners;

import org.bukkit.configuration.file.YamlConfiguration;

public class FileManager
{
	private Spawners plugin;
	public FileManager(Spawners plugin)
	{
		this.plugin = plugin;
	}  
	public YamlConfiguration loadConfig()
	{
		try 
	    {
			if (!plugin.getDataFolder().exists())
	        {
				plugin.getDataFolder().mkdirs();
	        }
	        if (!new File(plugin.getDataFolder(), "config.yml").exists())
	        {
	        	plugin.saveResource("config.yml", false);
	        }
	        return YamlConfiguration.loadConfiguration(new File(plugin.getDataFolder(), "config.yml"));     
	    } 
		catch (Exception ex) 
	    {
	        plugin.getLogger().severe("Blad podczas wczytywania konfiguracji!");
	        plugin.getServer().getPluginManager().disablePlugin(plugin);
	        return null;
	    }
	}	
	public YamlConfiguration loadMessages()
	{
		try 
	    {
			if (!plugin.getDataFolder().exists())
	        {
				plugin.getDataFolder().mkdirs();
	        }
	        if (!new File(plugin.getDataFolder(), "messages.yml").exists())
	        {
	        	plugin.saveResource("messages.yml", false);
	        }
	        return YamlConfiguration.loadConfiguration(new File(plugin.getDataFolder(), "messages.yml"));     
	    } 
		catch (Exception ex) 
	    {
	        plugin.getLogger().severe("Blad podczas wczytywania konfiguracji!");
	        plugin.getServer().getPluginManager().disablePlugin(plugin);
	        return null;
	    }
	}
}