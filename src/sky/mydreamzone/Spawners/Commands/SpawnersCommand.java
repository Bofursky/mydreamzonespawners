package net.myotherworld.Spawners.Commands;

import java.util.HashSet;

import net.myotherworld.Spawners.Spawners;

import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class SpawnersCommand implements CommandExecutor
{
	private Spawners plugin;
	public SpawnersCommand(Spawners plugin)
	{
		this.plugin = plugin;
	}
	public boolean onCommand(CommandSender sender, Command cmd, String commandLabel, String[] args) 
	{
		if (!(sender instanceof Player)) 
		{
			sender.sendMessage("Tylko gracz moze tego uzyc!");
			return true;
		}
		if(sender.hasPermission("MyOtherWorldSpawners.spawner")) 
		{
			Player p = (Player) sender;
			@SuppressWarnings("deprecation")
			Block b = p.getTargetBlock((HashSet<Byte>) null, 10);		
			if (args.length == 0) 
			{			
				if (b == null) 
				{
					p.sendMessage(plugin.message.Prefix + plugin.message.SpawnerLook);
					return true;
				}
				else if (!b.getType().equals(Material.MOB_SPAWNER)) 
				{
					p.sendMessage(plugin.message.Prefix + plugin.message.SpanwerWrong);
					return true;
				}
				else if (b.getType().equals(Material.MOB_SPAWNER)) 
				{
					plugin.Gui.SpawnerGuiOpen(p);
					return true;
				}
			}
			else
			{
				p.sendMessage(plugin.message.Prefix + plugin.message.SpawnerLook);
			}					
		}
	    else
	    {
	    	sender.sendMessage(plugin.message.Prefix + plugin.message.Permissions);
	    }
		return true;
	}
}
