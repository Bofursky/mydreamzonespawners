package net.myotherworld.Spawners.Commands;

import net.myotherworld.Spawners.Spawners;
import net.myotherworld.Spawners.Data.ConfigData;
import net.myotherworld.Spawners.Data.MessageData;

import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;

public class AdminCommand implements CommandExecutor
{
	private Spawners plugin;
	public AdminCommand(Spawners plugin)
	{
		this.plugin = plugin;
	}	
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args)
	{
		if(sender.hasPermission("MyOtherWorldPing.admin")) 
		{		
			if (args.length > 0) 				
			{			
				if(args[0].equalsIgnoreCase("Reload"))
				{	    
					if (args.length == 2) 				
					{
						if(args[1].equalsIgnoreCase("Config"))
						{
							if(sender.hasPermission("MyOtherWorldPrison.reload.config")) 
							{
								plugin.config = new ConfigData(plugin.fileManager.loadConfig());
						        sender.sendMessage("Poprawnie przeladowano config.yml.");
						        return true;
							}
						}
						else if(args[1].equalsIgnoreCase("Msg"))
						{
							if(sender.hasPermission("MyOtherWorldPrison.reload.message")) 
							{
								plugin.message = new MessageData(plugin.fileManager.loadMessages());
						        sender.sendMessage("Poprawnie przeladowano message.yml.");
						        return true;
							}	
						}
						else if(args[1].equalsIgnoreCase("All"))
						{
							if(sender.hasPermission("MyOtherWorldPrison.reload.all")) 
							{
								plugin.config = new ConfigData(plugin.fileManager.loadConfig());
								plugin.message = new MessageData(plugin.fileManager.loadMessages());
								sender.sendMessage("Poprawnie przeladowano wszystkie Cfg.");
								return true;
							}
						}
						else
						{
							sender.sendMessage(plugin.message.Prefix + plugin.message.NoCommands);
							return true;
						}	
					}
					for(String m : plugin.message.Admins) 
					{
						m = ChatColor.translateAlternateColorCodes('&', m);
						sender.sendMessage(plugin.message.Prefix + m);						
					}
					return true;
				}
				else
				{
					for(String m : plugin.message.Admins) 
					{
						m = ChatColor.translateAlternateColorCodes('&', m);
						sender.sendMessage(plugin.message.Prefix + m);					
					}
					return true;
				}
			}
			else
			{
				for(String m : plugin.message.Admins) 
				{
					m = ChatColor.translateAlternateColorCodes('&', m);
					sender.sendMessage(plugin.message.Prefix + m);				
				}	
				return true;
			}
		}
		else
		{
			sender.sendMessage(plugin.message.Prefix + plugin.message.Permissions);
		}
		return true;
	}
}
