package net.myotherworld.Spawners;

import net.milkbowl.vault.economy.Economy;
import net.myotherworld.Spawners.Commands.AdminCommand;
import net.myotherworld.Spawners.Commands.SpawnersCommand;
import net.myotherworld.Spawners.Data.ConfigData;
import net.myotherworld.Spawners.Data.MessageData;
import net.myotherworld.Spawners.Data.Listeners.SpawnersSetListener;
import net.myotherworld.Spawners.Managers.FileManager;

import org.bukkit.Bukkit;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.RegisteredServiceProvider;
import org.bukkit.plugin.java.JavaPlugin;

public class Spawners extends JavaPlugin
{
	public FileManager fileManager;
	public MessageData message;
	public ConfigData config;
	public SpawnersGui Gui;
	public Economy economy = null;
	public boolean mode1_8;
	
	public void onEnable()
	{
		version();
		config();
		commands();
		setupEconomy();
		Listener();
		
        if(!setupEconomy())
        {
            getLogger().severe("PLUGIN REQUIRES VAULT! COULD NOT SETUP ECONOMY! Vault 1.5.6-b49");
            Bukkit.getPluginManager().disablePlugin(this);
        }		
	}
	private void version()
	{
		String version = Bukkit.getServer().getClass().getPackage().getName().substring(Bukkit.getServer().getClass().getPackage().getName().lastIndexOf(".") + 1);
		if (version.startsWith("v1_8")) 
		{
			mode1_8 = true;
			getLogger().info("Turned on 1.8 mode.");
		}	
	}
	public void commands() 
	{
		getCommand("Spawners").setExecutor( new SpawnersCommand(this)); 
		getCommand("MowSpawners").setExecutor( new AdminCommand(this)); 	
	}
	public void config() 
	{
		fileManager = new FileManager(this);
		config = new ConfigData(fileManager.loadConfig());
		message = new MessageData(fileManager.loadMessages());
		Gui = new SpawnersGui(this);
	}
	public void Listener()
	{
		PluginManager pm = getServer().getPluginManager();
        pm.registerEvents(new SpawnersSetListener(this), this);
	}
	public boolean setupEconomy()
	{
	  RegisteredServiceProvider<Economy> economyProvider = Bukkit.getServer().getServicesManager().getRegistration(Economy.class);
	  if (economyProvider != null) 
	  {
	    this.economy = ((Economy)economyProvider.getProvider());
	  }
	  return this.economy != null;
	}

}
